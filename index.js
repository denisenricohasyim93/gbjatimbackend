var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var gatewayv0 = require('./src/v0/gateway')
var cors = require('cors')
app.use(cors())
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '20mb' }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
    res.header("Access-Control-Allow-Headers", "Content-Type")
    next();
});

app.get('/', function(req, res) {
   res.send({status : 200, message : 'Welcome To Guest Book Prov Jatim Backend'})
})

app.use('/v0', gatewayv0)

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Our app is running on port ${ PORT }`);
})

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
