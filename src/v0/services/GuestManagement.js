var express = require("express");
var router = express.Router();
var { emailAccount, sqlCredentials } = require("../credentials");
const fetch = require("node-fetch");
var moment = require("moment-timezone");

var mysql = require("mysql");
var pool = mysql.createPool(sqlCredentials);

// connection.connect();

router.get("/", function(req, res) {
  res.send({ status: 200, message: "Welcome to Guest Management" });
});

router.get("/allGuests", function(req, res) {
  pool.getConnection(function(err, connection) {
    if (err) return res.json({ status: 500, message: err });
    connection.query(
      "SELECT id, nama, no_telp, email, instagram,asal_kota, instansi, orang_yang_dituju, tanggal_berkunjung FROM guests",
      function(error, results, fields) {
        // if (error) throw error;
        // console.log('The solution is: ', results[0].solution);
        if (error) {
          res.send({ status: 501, message: error });
        } else {
          res.send({
            status: 200,
            message: "Anda berhasil dapatkan semua daftar tamu",
            data: results
          });
        }
      }
    );
  });
});

router.post("/addGuests", function(req, res) {
  var nama = req.body.nama;
  var no_telp = req.body.no_telp;
  var email = req.body.email;
  var instagram = req.body.instagram || "";
  var asal_kota = req.body.asal_kota;
  var instansi = req.body.instansi;
  var orang_yang_dituju = req.body.orang_yang_dituju;
  var profile_picture = req.body.profile_picture;
  var tanggal_berkunjung = moment()
    .tz("Asia/Jakarta")
    .format("DD-MM-YYYY HH:mm:ss");
  if (
    nama &&
    no_telp &&
    email &&
    // instagram &&
    asal_kota &&
    instansi &&
    orang_yang_dituju &&
    profile_picture &&
    tanggal_berkunjung
  ) {
    pool.getConnection(function(err, connection) {
      if (err) return res.json({ status: 500, message: err });
      connection.query(
        "INSERT INTO guests (nama, no_telp, email, instagram,asal_kota, instansi, orang_yang_dituju, profile_picture, tanggal_berkunjung) VALUES (" +
          JSON.stringify(nama) +
          "," +
          JSON.stringify(no_telp) +
          "," +
          JSON.stringify(email) +
          "," +
          JSON.stringify(instagram) +
          "," +
          JSON.stringify(asal_kota) +
          "," +
          JSON.stringify(instansi) +
          "," +
          JSON.stringify(orang_yang_dituju) +
          "," +
          JSON.stringify(profile_picture) +
          "," +
          JSON.stringify(tanggal_berkunjung) +
          ")",
        function(err, rows, fields) {
          if (!err) {
            var nodemailer = require("nodemailer");
            var inlineBase64 = require("nodemailer-plugin-inline-base64");
            var transporter = nodemailer.createTransport({
              service: "gmail",
              auth: {
                user: emailAccount.user,
                pass: emailAccount.pass
              }
            });
            const mailOptions = {
              from: emailAccount.user, // sender address
              to: email, // list of receivers
              subject: "Digital Guest Book Jawa Timur", // Subject line
              html: `
                <p>Ini adalah Foto Anda , Silahkan</p>
                <table
                  style="
                    width: 557px;
                    height:581px;
                    display:flex;
                    lign-items:center;
                    justify-content:center";
                    background: url(https://firebasestorage.googleapis.com/v0/b/faking-77d09.appspot.com/o/Overlay%20BPSDM%20Jatim%20(2).png?alt=media&token=00d5adbd-58d3-4927-abcd-ef8ee6349f2b) no-repeat center center fixed;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    padding : 0px;
                  "
                  bgcolor="black";
                  background='https://firebasestorage.googleapis.com/v0/b/faking-77d09.appspot.com/o/Overlay%20BPSDM%20Jatim%20(2).png?alt=media&token=00d5adbd-58d3-4927-abcd-ef8ee6349f2b';
                >
                  <img 
                    src=${JSON.stringify(profile_picture)}
                    style="
                      width: 550px;
                      height:410px; 
                      margin-top:65px;
                      margin-left: 0px;
                      margin-right: 0px;
                      margin-bottom : 0px;
                    "
                  />
                </table>
              `
            };
            transporter.use(
              "compile",
              inlineBase64({ cidPrefix: "digiguesbookjatim_" })
            );
            transporter.sendMail(mailOptions, function(err, info) {
              if (err) {
                res.send({
                  status: 501,
                  message: "Gagal Mengirim Gambar via E-Mail",
                  err: err
                });
              } else {
                res.send({
                  status: 200,
                  message:
                    "Anda Berhasil Menambah Data Kunjungan. Silahkan Cek E-Mail Anda (" +
                    email +
                    ") untuk melihat hasil foto",
                  result: rows
                });
              }
            });
          } else {
            res.send({ status: 502, message: err });
          }
        }
      );
    });
  } else {
    res.send({ status: 503, message: "Field Tidak Lengkap" });
  }
});

router.post("/deleteGuests", function(req, res) {
  var id = req.body.id;
  if (id) {
    pool.getConnection(function(err, connection) {
      if (err) return res.json({ status: 500, message: err });
      connection.query("DELETE FROM guests WHERE id = " + id, function(
        err,
        rows,
        fields
      ) {
        if (rows) {
          res.send({ status: 200, message: "Data berhasil di delete" });
        } else {
          res.send({ status: 501, message: "Data gagal di delete" });
        }
      });
    });
  } else {
    res.send({ status: 502, message: "Field Tidak Lengkap" });
  }
});

// connection.end();

module.exports = router;
