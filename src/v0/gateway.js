var express = require('express')
var router = express.Router()
var GuestManagement = require('./services/GuestManagement');

router.get('/', function (req, res) {
    res.send({status : 200, message : 'Welcome to TNC Backend v0'})
})

router.use('/GM', GuestManagement)

module.exports = router